package Time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("The time provided is not valid");
	}

	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:00");
		assertTrue("The time provided does not match the result", totalSeconds == 0);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondaryBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("The time provided is not valid");
	}

	 @Test public void testGetMilliseconds() 
	 { 
		 int totalMilliseconds = Time.getMilliseconds("00:00:00:001"); 
		 assertTrue("The time provided does not match the result", totalMilliseconds == 1);
	 }

	 @Test (expected = NumberFormatException.class) 
	 public void testGetMillisecondsException() 
	 { 
		 int totalMilliseconds = Time.getMilliseconds("00:00:00:00A"); 
		 fail("The Time provided is not valid");
	 }
	
	 @Test 
	 public void testGetMillisecondsBoundaryIn()
	 {
		 int totalMilliseconds = Time.getMilliseconds("00:00:00:000");
		 assertTrue("The time provided does not match the result", totalMilliseconds == 0);
	 }
	 
	 @Test (expected = NumberFormatException.class)
	 public void testGetMillisecondsBoundaryOut() 
	 {
		int totalMilliseconds = Time.getMilliseconds("01:01:60:1000");
		fail("The time provided is not valid");
	 }
}
